function ProdutosDAO(db) {
  this._db = db();
  this._Produtos = this._db.Mongoose.model(
    'produtos',
    this._db.ProdutoSchema,
    'produtos'
  );
}

ProdutosDAO.prototype.lista = function(callback) {
  this._Produtos.find({}, callback);
};

ProdutosDAO.prototype.listaId = function(id, callback) {
  this._Produtos.findById(id, function(err, doc) {
    if (err) {
      return callback({ error: 'Error! ' + err.message });
    }
    if (!doc) {
      return callback({ error: 'Error! documento não encontrado' });
    }
    return callback(doc);
  });
};

ProdutosDAO.prototype.salva = function(body, callback) {

  var produto = new this._Produtos({
    nome: body.nome,
    descricao: body.descricao,
    imagem: body.imagem,
    valor: body.valor,
    fator: body.fator
  });

  produto.save(function(err, dados) {
    if (err) {
      return callback({ error: 'Error! ' + err.message });
    }
    return callback({ message: 'Produto adicionado com Sucesso!', produto: dados});
  });
};

ProdutosDAO.prototype.altera = function(id, body, callback) {
  this._Produtos.findById(id, function(err, doc) {
    if (err) {
      return callback({ error: 'Error! ' + err.message });
    }
    if (!doc) {
      return callback({ error: 'Error! documento não encontrado' });
    }
    doc.nome = body.nome || doc.nome;
    doc.descricao = body.descricao || doc.descricao;
    doc.imagem = body.imagem || doc.imagem;
    doc.valor = body.valor || doc.valor;
    doc.fator = body.fator || doc.fator;
    doc.save();
    return callback({ message: 'Produto Atualizado com Sucesso!', produto: doc});
  });
};

ProdutosDAO.prototype.deleta = function(id, callback) {
  this._Produtos.findById(id, function(err, doc) {
    if (err) {
      return callback({ error: 'Error! ' + err.message });
    }
    if (!doc) {
      return callback({ error: 'Error! documento não encontrado' });
    }
    doc.remove();
    return callback({ message: 'Produto excluído com Sucesso!' });
  });
};

module.exports = ProdutosDAO;
