#Desafio Backend

##Frameworks utilizados
####Estrutura
	-NodeJs
	-Express
####CRUD
	-Mongodb
	-Mongoose
	-PagarMe
####Testes Unitarios
	-Mocha
	-Chai

## Subindo o projeto
####Pré requisito
	-MongoDB: Instalar a aplicação do mongodb
	-mongod: Executar na pasta \bin da instalação do MongoDB o comando no prompt: mongod --port 27017 --dbpath <caminhoprojeto>/data
####Projeto desafio
	- Server: Na raiz do projeto executar o comando no prompt: npm start
####Testes Unitarios
	-Mocha: Na raiz do projeto executar o comando no prompt: npm test
	
##APIs disponiveis:
###Produtos

GET - /produtos
res
```
[
    {
        "_id": "5ba2e8171e73aa282041501d",
        "nome": "ProdA",
        "descricao": "",
        "imagem": "",
        "valor": 100,
        "fator": "A",
        "__v": 0
    },
	...
    {
        "_id": "5ba2e8a81e73aa2820415022",
        "nome": "ProdE",
        "descricao": "",
        "imagem": "",
        "valor": 600,
        "fator": "C",
        "__v": 0
    }
]
```

POST - /produtos
body
```
{
	"nome": "ProdC",
	"descricao": "Descricao",
	"imagem": "",
	"valor": 400,
	"fator": "B"
}
```
res
```
{
    "message": "Produto adicionado com Sucesso!",
    "produto": {
        "_id": "5ba2e83d1e73aa282041501f",
        "nome": "ProdC",
        "descricao": "Descricao",
        "imagem": "",
        "valor": 400,
        "fator": "B",
        "__v": 0
    }
}
```

PUT - /produtos/:id
body
```
{
	"nome": "ProdZ"
}
```
res
```
{
    "message": "Produto Atualizado com Sucesso!",
    "produto": {
        "_id": "5ba2e8951e73aa2820415020",
        "nome": "ProdZ",
        "descricao": "",
        "imagem": "",
        "valor": 400,
        "fator": "B",
        "__v": 0
    }
}
```

DELETE - /produtos/:id
res
```
{
    "message": "Produto excluído com Sucesso!"
}
```

###Carrinhos

GET - /carrinhos
res
```
[
    {
        "_id": "5ba2ea0e1e73aa2820415023",
        "produtos": [],
        "desconto": 0,
        "total": 0,
        "__v": 2
    }
]
```

POST - /carrinhos
body
```
{
	"produtos": [
	]
}
```
res
```
{
    "message": "Carrinho adicionado com Sucesso!",
    "carrinho": {
        "_id": "5ba2ea0e1e73aa2820415023",
        "produtos": [],
        "desconto": 0,
        "total": 0,
        "__v": 0
    }
}
```

PUT - /carrinhos/:id/add
body
```
{
	"produtos": [
		{
			"produto": {
				"id": "5ba2e8951e73aa2820415020"
			},
			"quantidade": 1
		}
	]
}
```
res
```
{
    "message": "Carrinho Atualizado com Sucesso!",
    "carrinho": {
        "_id": "5ba2ea0e1e73aa2820415023",
        "produtos": [
            {
                "produto": {
                    "_id": "5ba2e8951e73aa2820415020",
                    "nome": "ProdZ",
                    "descricao": "",
                    "imagem": "",
                    "valor": 380,
                    "fator": "B",
                    "__v": 0
                },
                "quantidade": 1
            }
        ],
        "desconto": 5,
        "total": 380,
        "__v": 0
    }
}
```

PUT - /carrinhos/:id/remove
body
```
{
	"produtos": [
		{
			"produto": {
				"id": "5ba2e8951e73aa2820415020"
			},
			"quantidade": 1
		}
	]
}
```
res
```
{
    "message": "Carrinho Atualizado com Sucesso!",
    "carrinho": {
        "_id": "5ba2ea0e1e73aa2820415023",
        "produtos": [],
        "desconto": 0,
        "total": 0,
        "__v": 1
    }
}
```

DELETE - /carrinhos/:id
res
```
{
    "message": "Carrinho excluído com Sucesso!"
}
```

GET - /checkout/:id
res
```
{
    "object": "transaction",
    "status": "refused",
    "refuse_reason": "antifraud",
	...
}
```

###Resultado do Teste
```
 /GET carrinho
GET /carrinhos 200 13.984 ms - 2
    √ Deve retornar todos os carrinhos (40ms)

  /POST carrinho
POST /carrinhos 200 41.963 ms - 137
    √ Deve Criar um carrinho (49ms)

  /GET/:id carrinho
    √ Deve retornar um carrinho dado o id

  /PUT/:id/ADD or REMOVE produtos no carrinho
GET /carrinhos/5ba3c06029a92d32a0523db0 200 1022.611 ms - 79
PUT /carrinhos/5ba3c06029a92d32a0523db2/add 200 1043.561 ms - 318
    √ Deve adicionar um produto a um carrinho dado o id (1067ms)
PUT /carrinhos/5ba3c06129a92d32a0523db5/add 200 6.606 ms - 320
PUT /carrinhos/5ba3c06129a92d32a0523db5/remove 200 4.660 ms - 137
    √ Deve remover um produto de um carrinho dado o id

  /DELETE/:id carrinho
DELETE /carrinhos/5ba3c06129a92d32a0523db8 200 2.306 ms - 45
    √ Deve excluir um carrinho dado o id

  /GET produto
GET /produtos 200 4.108 ms - 311
    √ Deve retornar todos os produtos

  /POST produto
POST /produtos 200 1.967 ms - 68
    √ Não deve retornar o POST do produto criado, uma vez que não foi definido o campo: nome
POST /produtos 200 2.289 ms - 164
    √ Deve Criar um produto

  /GET/:id produto
    √ Deve retornar um produto dado o id

  /PUT/:id produto
GET /produtos/5ba3c06129a92d32a0523dbb 200 5.667 ms - 108
PUT /produtos/5ba3c06129a92d32a0523dbc 200 6.048 ms - 164
    √ Deve atualizar um produto dado o id

  /DELETE/:id produto
DELETE /produtos/5ba3c06129a92d32a0523dbd 200 2.328 ms - 44
    √ Deve excluir um produto dado o id


  12 passing (2s)
```