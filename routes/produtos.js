var express = require('express');
var router = express.Router();
var db = require('../infra/db');
var ProdutosDAO = require('../infra/ProdutosDAO');

/* GET users listing. */
router.get('/', function(req, res, next) {
  var conn = db();
  var prod = new ProdutosDAO(conn);

  prod.lista(function(err, dados) {
    res.json(dados);
  });
});

router.get('/:id', function(req, res, next) {
  var conn = db();
  var prod = new ProdutosDAO(conn);
  var id = req.params.id;

  prod.listaId(id, function(dados) {
    res.json(dados);
  });
});

router.post('/', function(req, res, next) {
  var conn = db();
  var prod = new ProdutosDAO(conn);

  prod.salva(req.body, function(dados) {
    res.json(dados);
  });
});

router.put('/:id', function(req, res, next) {
  var conn = db();
  var prod = new ProdutosDAO(conn);
  var id = req.params.id;

  prod.altera(id, req.body, function(dados) {
    res.json(dados);
  });
});

router.delete('/:id', function(req, res, next) {
  var conn = db();
  var prod = new ProdutosDAO(conn);
  var id = req.params.id;

  prod.deleta(id, function(dados) {
    res.json(dados);
  });
});

module.exports = router;
