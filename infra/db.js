var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/dasafiobackend');

var produtoSchema = new mongoose.Schema(
  {
    nome: { type: String, required: true},
    descricao: String,
    imagem: String,
    valor: Number,
    fator: String
  },
  { collection: 'produtos' }
);

var carrinhoSchema = new mongoose.Schema(
  {
    produtos: [{ produto: produtoSchema, quantidade: Number, _id: false }],
    desconto: Number,
    total: Number
  },
  { collection: 'carrinhos' }
);

function dbConnection() {
  return {
    Mongoose: mongoose,
    ProdutoSchema: produtoSchema,
    CarrinhoSchema: carrinhoSchema
  };
}

module.exports = function() {
  return dbConnection;
};
