var express = require('express');
var router = express.Router();
var db = require('../infra/db');
var CarrinhosDAO = require('../infra/CarrinhosDAO');

router.get('/', function(req, res, next) {
  var conn = db();
  var car = new CarrinhosDAO(conn);

  car.lista(function(erros, dados) {
    res.json(dados);
  });
});

router.get('/:id', function(req, res, next) {
  var conn = db();
  var car = new CarrinhosDAO(conn);
  var id = req.params.id;

  car.listaId(id, function(dados) {
    res.json(dados);
  });
});

router.post('/', function(req, res, next) {
  var conn = db();
  var car = new CarrinhosDAO(conn);

  car.salva(req.body, function(dados) {
    res.json(dados);
  });
});

router.put('/:id/add', function(req, res, next) {
  var conn = db();
  var car = new CarrinhosDAO(conn);
  var id = req.params.id;

  car.add(id, req.body, function(dados) {
    res.json(dados);
  });
});

router.put('/:id/remove', function(req, res, next) {
  var conn = db();
  var car = new CarrinhosDAO(conn);
  var id = req.params.id;

  car.remove(id, req.body, function(dados) {
    res.json(dados);
  });
});

router.delete('/:id', function(req, res, next) {
  var conn = db();
  var car = new CarrinhosDAO(conn);
  var id = req.params.id;

  car.deleta(id, function(dados) {
    res.json(dados);
  });
});

module.exports = router;
