var db = require('../infra/db');
var CarrinhosDAO = require('../infra/CarrinhosDAO');
var pagarme = require('pagarme');

pagar = async function(id, callback){
  var conn = db();
  var car = new CarrinhosDAO(conn);

  let carrinho = await car.listaId(id, dados => carrinho = dados);

  let client = await pagarme.client.connect({
    api_key: 'ak_test_Fdo1KyqBTdnTFeLgBhkgRcgm9hwdzd'
  });

  let data = await bodyPagar(carrinho);
  data = await client.transactions.create(data);

  return callback(data);
};

exports.pagar = pagar;

bodyPagar = async function(data) {
  let itens = [];

  data.produtos.forEach(elprod => {
    itens.push({
      id: elprod.produto._id,
      title: elprod.produto.nome,
      unit_price: elprod.produto.valor,
      quantity: elprod.quantidade,
      tangible: true
    });
  });

  return {
    amount: 210000, //data.total,
    card_number: '4111111111111111',
    card_cvv: '123',
    card_expiration_date: '0922',
    card_holder_name: 'João das Neves',
    customer: {
      external_id: '#3311',
      name: 'João das Neves Braulio',
      type: 'individual',
      country: 'br',
      email: 'joaodasneves@got.com',
      documents: [
        {
          type: 'cpf',
          number: '00000000000'
        }
      ],
      phone_numbers: ['+5511999998888', '+5511888889999'],
      birthday: '1965-01-01'
    },
    billing: {
      name: 'João das Neves',
      address: {
        country: 'br',
        state: 'sp',
        city: 'Cotia',
        neighborhood: 'Rio Cotia',
        street: 'Rua Matrix',
        street_number: '9999',
        zipcode: '06714360'
      }
    },
    shipping: {
      name: 'Neo Reeves',
      fee: 1000,
      delivery_date: '2000-12-21',
      expedited: true,
      address: {
        country: 'br',
        state: 'sp',
        city: 'Cotia',
        neighborhood: 'Rio Cotia',
        street: 'Rua Matrix',
        street_number: '9999',
        zipcode: '06714360'
      }
    },
    items: itens
  };
};
