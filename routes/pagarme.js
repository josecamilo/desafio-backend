var express = require('express');
var router = express.Router();
var ctrlpagarme = require('../controller/pagarme');

router.get('/:id', function(req, res, next) {
  var id = req.params.id;

  ctrlpagarme.pagar(id, function(dados) {
    res.json(dados);
  });
});

module.exports = router;
