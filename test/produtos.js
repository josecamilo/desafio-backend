var mongoose = require('mongoose');
var db = require('../infra/db');
var conn = db();
var Produtos = conn().Mongoose.model(
  'produtos',
  conn().ProdutoSchema,
  'produtos'
);

var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../app');
var should = chai.should();

chai.use(chaiHttp);

before(function(done) {
  Produtos.remove({}, function(error) {
    done();
  });
});

after(function(done) {
  Produtos.remove({}, function(error) {
    done();
  });
});

/**
 * Teste da rota: /GET
 */
describe('/GET produto', function() {
  it('Deve retornar todos os produtos', function(done) {
    chai
      .request(server)
      .get('/produtos')
      .end(function(error, res) {
        res.should.have.status(200);
        res.body.should.be.a('array');
        //res.body.length.should.be.eql(0);
        done();
      });
  });
});

/**
 * Teste da rota: /POST
 */
describe('/POST produto', function() {
  it('Não deve retornar o POST do produto criado, uma vez que não foi definido o campo: nome', function(done) {
    var produto = {
      descricao: '',
      imagem: '',
      valor: 100,
      fator: 'A'
    };

    chai
      .request(server)
      .post('/produtos')
      .send(produto)
      .end(function(error, res) {
        res.should.have.status(200);
        res.body.should.be.a('object');
        res.body.should.have.property('error');
        done();
      });
  });

  it('Deve Criar um produto', function(done) {
    var produto = {
      nome: 'ProdA',
      descricao: '',
      imagem: '',
      valor: 100,
      fator: 'A'
    };
    chai
      .request(server)
      .post('/produtos')
      .send(produto)
      .end(function(error, res) {
        res.should.have.status(200);
        res.body.should.be.a('object');
        res.body.should.have
          .property('message')
          .eql('Produto adicionado com Sucesso!');
        res.body.produto.should.have.property('nome');
        res.body.produto.should.have.property('descricao');
        res.body.produto.should.have.property('imagem');
        res.body.produto.should.have.property('valor');
        res.body.produto.should.have.property('fator');
        done();
      });
  });
});

/**
 * Teste da rota: /GET/:id
 */
describe('/GET/:id produto', function() {
  it('Deve retornar um produto dado o id', function() {
    var produto = new Produtos({
      nome: 'ProdA',
      descricao: '',
      imagem: '',
      valor: 100,
      fator: 'A'
    });
    produto.save(function(error, produto) {
      chai
        .request(server)
        .get('/produtos/' + produto.id)
        .send(produto)
        .end(function(error, res) {
          res.should.be.a('object');
          res.body.should.have.property('nome');
          res.body.should.have.property('descricao');
          res.body.should.have.property('imagem');
          res.body.should.have.property('valor');
          res.body.should.have.property('fator');
          res.body.should.have.property('_id').eql(produto.id);
        });
    });
  });
});

/**
 * Teste da rota: /PUT/:id
 */
describe('/PUT/:id produto', function() {
  it('Deve atualizar um produto dado o id', function(done) {
    var produto = new Produtos({
      nome: 'ProdA',
      descricao: '',
      imagem: '',
      valor: 100,
      fator: 'A'
    });
    produto.save(function(error, produto) {
      chai
        .request(server)
        .put('/produtos/' + produto.id)
        .send({
          nome: 'ProdA',
          descricao: '',
          imagem: '',
          valor: 300,
          fator: 'A'
        })
        .end(function(error, res) {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have
            .property('message')
            .eql('Produto Atualizado com Sucesso!');
          res.body.produto.should.have.property('valor').eql(300);
          done();
        });
    });
  });
});

/**
 * Teste da rota: /DELETE/:id
 */
describe('/DELETE/:id produto', function() {
  it('Deve excluir um produto dado o id', function(done) {
    var produto = new Produtos({
      nome: 'ProdA',
      descricao: '',
      imagem: '',
      valor: 100,
      fator: 'A'
    });
    produto.save(function(error, produto) {
      chai
        .request(server)
        .delete('/produtos/' + produto.id)
        .end(function(error, res) {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have
            .property('message')
            .eql('Produto excluído com Sucesso!');
          done();
        });
    });
  });
});
