function CarrinhosDAO(db) {
  this._db = db();
  this._Carrinhos = this._db.Mongoose.model(
    'carrinhos',
    this._db.CarrinhoSchema,
    'carrinhos'
  );
  this._Produtos = this._db.Mongoose.model(
    'produtos',
    this._db.ProdutoSchema,
    'produtos'
  );
}

CarrinhosDAO.prototype.lista = function(callback) {
  this._Carrinhos.find({}, callback);
};

CarrinhosDAO.prototype.listaId = function(id, callback) {
  this._Carrinhos.findById(id, function(err, doc) {
    if (err) {
      return callback({ error: 'Error! ' + err.message });
    }
    if (!doc) {
      return callback({ error: 'Error! documento não encontrado' });
    }
    return callback(doc);
  });
};

CarrinhosDAO.prototype.findProdById = function(id) {
  return new Promise((resolve, reject) => {
    this._Produtos.findById(id, function(err, doc) {
      if (err) {
        resolve({ error: 'Error! ' + err.message });
      }
      if (!doc) {
        resolve({ error: 'Error! documento não encontrado' });
      }
      resolve(doc);
    });
  });
};

CarrinhosDAO.prototype.findCarrById = function(id) {
  return new Promise((resolve, reject) => {
    this._Carrinhos.findById(id, function(err, doc) {
      if (err) {
        resolve({ error: 'Error! ' + err.message });
      }
      if (!doc) {
        resolve({ error: 'Error! documento não encontrado' });
      }
      resolve(doc);
    });
  });
};

CarrinhosDAO.prototype.salva = async function(body, callback) {
  var dados = await this.carrinho(body);

  var carrinho = new this._Carrinhos({
    produtos: dados.produtos,
    desconto: dados.desconto,
    total: dados.total
  });

  carrinho.save(function(err, dados) {
    if (err) {
      return callback({ error: 'Error! ' + err.message });
    }
    return callback({message: 'Carrinho adicionado com Sucesso!', carrinho: dados});
  });
};

CarrinhosDAO.prototype.add = async function(id, body, callback) {
  var doc = await this.findCarrById(id);
  var carrinho = {
    id: id,
    produtos: []
  };

  // produtos ja existentes
  doc.produtos.forEach(element => {
    carrinho.produtos.push(element);
  });

  // adiciona produtos novos
  body.produtos.forEach(element => {
    var index = carrinho.produtos.findIndex(
      o => o.produto.id === element.produto.id
    );
    index > -1
      ? (carrinho.produtos[index].quantidade += element.quantidade)
      : carrinho.produtos.push(element);
  });

  var dados = await this.carrinho(carrinho);

  doc.produtos = dados.produtos;
  doc.desconto = dados.desconto;
  doc.total = dados.total;
  doc.save();

  callback({message: 'Carrinho Atualizado com Sucesso!', carrinho: doc});
};

CarrinhosDAO.prototype.remove = async function(id, body, callback) {
  var doc = await this.findCarrById(id);
  var carrinho = {
    id: id,
    produtos: []
  };

  // produtos ja existentes
  doc.produtos.forEach(element => {
    carrinho.produtos.push(element);
  });

  // remove produtos solicitados
  body.produtos.forEach(element => {
    var index = carrinho.produtos.findIndex(
      o => o.produto.id === element.produto.id
    );
    if (index > -1) {
      carrinho.produtos[index].quantidade -= element.quantidade;

      if (carrinho.produtos[index].quantidade <= 0) {
        carrinho.produtos.splice(index, 1);
      }
    }
  });

  var dados = await this.carrinho(carrinho);

  doc.produtos = dados.produtos;
  doc.desconto = dados.desconto;
  doc.total = dados.total;
  doc.save();

  callback({message: 'Carrinho Atualizado com Sucesso!', carrinho: doc});
};

CarrinhosDAO.prototype.deleta = function(id, callback) {
  this._Carrinhos.findById(id, function(err, doc) {
    if (err) {
      return callback({ error: 'Error! ' + err.message });
    }
    if (!doc) {
      return callback({ error: 'Error! documento não encontrado' });
    }
    doc.remove();
    return callback({ message: 'Carrinho excluído com Sucesso!' });
  });
};

CarrinhosDAO.prototype.carrinho = async function(body) {
  var produtos = [];
  var fator = [
    { tipo: 'A', desconto: 1, limite: 5, count: 0 },
    { tipo: 'B', desconto: 5, limite: 15, count: 0 },
    { tipo: 'C', desconto: 10, limite: 30, count: 0 }
  ];
  var descontoLimite = 30;
  var descontoCarrinho = 0;
  var totalCarrinho = 0;

  for (let i = 0; i < body.produtos.length; i++) {
    const element = body.produtos[i];
    var obj = await this.findProdById(element.produto.id);
    produtos.push({ produto: obj, quantidade: element.quantidade });
  }

  // analisa fator dos produtos
  for (let i = 0; i < produtos.length; i++) {
    const elproduto = produtos[i];
    for (let n = 0; n < fator.length; n++) {
      const elfator = fator[n];
      elfator.count +=
        elproduto.produto.fator === elfator.tipo ? elproduto.quantidade : 0;
    }
  }

  // analisa desconto dos produtos
  fator.forEach(elfator => {
    descontoCarrinho +=
      elfator.count <= elfator.limite
        ? elfator.desconto * elfator.count
        : elfator.desconto * elfator.limite;
  });

  // analisa desconto limite do carrinho
  descontoCarrinho =
    descontoCarrinho > descontoLimite ? descontoLimite : descontoCarrinho;

  // aplica desconto nos produtos do carrinho
  produtos.forEach(elproduto => {
    elproduto.produto.valor -=
      (elproduto.produto.valor * descontoCarrinho) / 100;
    totalCarrinho += elproduto.produto.valor * elproduto.quantidade;
  });

  var carrinho = {
    produtos: produtos,
    desconto: descontoCarrinho,
    total: totalCarrinho
  };

  return carrinho;
};

module.exports = CarrinhosDAO;
