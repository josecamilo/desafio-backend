var mongoose = require('mongoose');
var db = require('../infra/db');
var conn = db();
var Carrinhos = conn().Mongoose.model(
  'carrinhos',
  conn().CarrinhoSchema,
  'carrinhos'
);
var Produtos = conn().Mongoose.model(
    'produtos',
    conn().ProdutoSchema,
    'produtos'
  );

var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../app');
var should = chai.should();

chai.use(chaiHttp);

before(function(done) {
  Produtos.remove({}, function(error) {
    done();
  });
});

after(function(done) {
  Produtos.remove({}, function(error) {
    done();
  });
});

before(function(done) {
  Carrinhos.remove({}, function(error) {
    done();
  });
});

after(function(done) {
  Carrinhos.remove({}, function(error) {
    done();
  });
});

/**
 * Teste da rota: /GET
 */
describe('/GET carrinho', function() {
  it('Deve retornar todos os carrinhos', function(done) {
    chai
      .request(server)
      .get('/carrinhos')
      .end(function(error, res) {
        res.should.have.status(200);
        res.body.should.be.a('array');
        //res.body.length.should.be.eql(0);
        done();
      });
  });
});

/**
 * Teste da rota: /POST
 */
describe('/POST carrinho', function() {
  it('Deve Criar um carrinho', function(done) {
    var carrinho = {
      produtos: []
    };
    chai
      .request(server)
      .post('/carrinhos')
      .send(carrinho)
      .end(function(error, res) {
        res.should.have.status(200);
        res.body.should.be.a('object');
        res.body.should.have
          .property('message')
          .eql('Carrinho adicionado com Sucesso!');
        res.body.carrinho.should.have.property('produtos');
        res.body.carrinho.should.have.property('desconto');
        res.body.carrinho.should.have.property('total');
        done();
      });
  });
});

/**
 * Teste da rota: /GET/:id
 */
describe('/GET/:id carrinho', function() {
  it('Deve retornar um carrinho dado o id', function() {
    var carrinho = new Carrinhos({
      produtos: [],
      desconto: 0,
      total: 0
    });
    carrinho.save(function(error, carrinho) {
      chai
        .request(server)
        .get('/carrinhos/' + carrinho.id)
        .send(carrinho)
        .end(function(error, res) {
          res.should.be.a('object');
          res.body.should.have.property('produtos');
          res.body.should.have.property('desconto');
          res.body.should.have.property('total');
          res.body.should.have.property('_id').eql(carrinho.id);
        });
    });
  });
});

/**
 * Teste da rota: /PUT/:id/ADD
 */
describe('/PUT/:id/ADD or REMOVE produtos no carrinho', function() {
  it('Deve adicionar um produto a um carrinho dado o id', function(done) {
    var produto = new Produtos({
      nome: 'Produto TST11001',
      descricao: 'Produto gerado por testes unitarios',
      imagem: '',
      valor: 100,
      fator: 'A'
    });
    produto.save(function(error, produto) {
      var carrinho = new Carrinhos({
        produtos: []
      });
      carrinho.save(function(error, carrinho) {
        chai
          .request(server)
          .put('/carrinhos/' + carrinho.id + '/add')
          .send({
            produtos: [
              {
                produto: {
                  id: produto.id
                },
                quantidade: 1
              }
            ]
          })
          .end(function(error, res) {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have
              .property('message')
              .eql('Carrinho Atualizado com Sucesso!');
            res.body.carrinho.produtos.should.be.a('array');
            res.body.carrinho.produtos.length.should.be.eql(1);
            done();
          });
      });
    });
  });

  it('Deve remover um produto de um carrinho dado o id', function(done) {
    var produto = new Produtos({
      nome: 'Produto TST11002',
      descricao: 'Produto gerado por testes unitarios',
      imagem: '',
      valor: 200,
      fator: 'B'
    });
    produto.save(function(error, produto) {
      var carrinho = new Carrinhos({
        produtos: []
      });
      carrinho.save(function(error, carrinho) {
        chai
          .request(server)
          .put('/carrinhos/' + carrinho.id + '/add')
          .send({
            produtos: [
              {
                produto: {
                  id: produto.id
                },
                quantidade: 1
              }
            ]
          })
          .end(function(error, res) {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have
              .property('message')
              .eql('Carrinho Atualizado com Sucesso!');
            res.body.carrinho.produtos.should.be.a('array');
            res.body.carrinho.produtos.length.should.be.eql(1);

            chai
              .request(server)
              .put('/carrinhos/' + carrinho.id + '/remove')
              .send({
                produtos: [
                  {
                    produto: {
                      id: produto.id
                    },
                    quantidade: 1
                  }
                ]
              })
              .end(function(error, res) {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have
                  .property('message')
                  .eql('Carrinho Atualizado com Sucesso!');
                res.body.carrinho.produtos.should.be.a('array');
                res.body.carrinho.produtos.length.should.be.eql(0);
                done();
              });
          });
      });
    });
  });
});

/**
 * Teste da rota: /DELETE/:id
 */
describe('/DELETE/:id carrinho', function() {
  it('Deve excluir um carrinho dado o id', function(done) {
    var carrinho = new Carrinhos({
      produtos: []
    });
    carrinho.save(function(error, carrinho) {
      chai
        .request(server)
        .delete('/carrinhos/' + carrinho.id)
        .end(function(error, res) {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have
            .property('message')
            .eql('Carrinho excluído com Sucesso!');
          done();
        });
    });
  });
});
